// Syntax: npx create-react-app "project-name"

// after creating project execute the command "npm start" to run our project in our localhost

// after that...we delete the unnecessary files from the newly created project
	// 1. App.test.js
	// 2. index.css
	// 3. logo.svg
	// 4. reportWebVitals.js

// after we deleted the unnecessary files, we encountered errors.
	// errors encountered: Imports from the deleted files
	// to fix it, we remove the syntax or codes of the imported files

// now we have a blank slate where we can start building our own ReactJS app.

// Important Note:
	// similar to NodeJS and expressJS, we can install packages in our react application to make the woek easier for us
		// example: npm install react-bootstrap bootstrap

	// "import" statement allows us to use the code/exported modules from files similar to how we use the require keyword up a NodeJS

	// ReactJS components are independent, reusable

// Storing informatin in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop.

// All information provide to the Provider component can be acessed later on from the context object as properties