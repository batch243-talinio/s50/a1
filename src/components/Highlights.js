// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){

	return(
		<Row className = "my-3">
		{/* First Card*/}
			<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Learn From Home</h2>
			        </Card.Title>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse cursus neque lectus, nec rhoncus eros aliquam eu. Ut ac magna dignissim orci interdum iaculis. Donec tempus ultricies ex vitae bibendum. Maecenas id ipsum nisi. Donec efficitur id nibh vel blandit. Aliquam vitae tincidunt odio. Duis quam ex, faucibus ac quam sit amet, aliquam scelerisque mauris. Fusce eget nisi nulla. Nullam sit amet neque ex. Fusce vitae sapien ac eros interdum egestas a a eros.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			{/* Second Card*/}
			<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Study Now, Pay Later</h2>
			        </Card.Title>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse cursus neque lectus, nec rhoncus eros aliquam eu. Ut ac magna dignissim orci interdum iaculis. Donec tempus ultricies ex vitae bibendum. Maecenas id ipsum nisi. Donec efficitur id nibh vel blandit. Aliquam vitae tincidunt odio. Duis quam ex, faucibus ac quam sit amet, aliquam scelerisque mauris. Fusce eget nisi nulla. Nullam sit amet neque ex. Fusce vitae sapien ac eros interdum egestas a a eros.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

		{/* Third Card*/}
			<Col xs = {12} md = {4}>
			<Card className = "cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Be part of our Community</h2>
			        </Card.Title>
			        <Card.Text>
			          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse cursus neque lectus, nec rhoncus eros aliquam eu. Ut ac magna dignissim orci interdum iaculis. Donec tempus ultricies ex vitae bibendum. Maecenas id ipsum nisi. Donec efficitur id nibh vel blandit. Aliquam vitae tincidunt odio. Duis quam ex, faucibus ac quam sit amet, aliquam scelerisque mauris. Fusce eget nisi nulla. Nullam sit amet neque ex. Fusce vitae sapien ac eros interdum egestas a a eros.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>


		</Row>
		)
}