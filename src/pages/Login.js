import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Container, Row, Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';


export default function Login(){
	// State hooks to store the values of the input field from our user
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	
	const [isActive, setIsActive] = useState(false)

	//const [user, setUser] = useState(null);
	// allows us to consume the User context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
		if(email !== '' && password1 !== ''){
			return setIsActive(true)
		}
		setIsActive(false)
		// console.log(email);
		// console.log(password1);
		
	}, [email, password1])

	function loginUser(event){
		event.preventDefault();
		alert(`Congratulations ${email}, you are now logged in!`)

		// we are Setting the email of the authenticated user in the local Storage(WEB)
		// Syntax:
			// localStorage.setItem('propertyName', value)

		// Storing information in the local storage will make the data persistent even as the page is refreshed unlike the use of states where informayions is reset when refreshing the page.
		// set the global user state to have properties obetained from the local storage

		// though access to the user information can be done via the localStorage this is necessary to update the user state which will help update the app component and rerender it to avoid refreshing the page upon user login and log out.
		localStorage.setItem('email', email);

		setUser(localStorage.getItem("email"));

		setEmail('')
		setPassword1('')
	}

	return (
		(user !== null) ?
			<Navigate to = "/" />
		:
		<Container>
		<Row>
			<Col className = "col-md-6 col-8 offset-md-3 offset-2">

		        <h6>Zuitt Booking</h6>
		        <h3>Login</h3>



				<Form onSubmit = {loginUser} className = "bg-light">
				      <Form.Group className="mb-3" controlId="email">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
					        type="email" 
					        placeholder="Enter email"
					        value = {email} 
					        onChange = {event => setEmail(event.target.value)}
					        required
					        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Enter desired password</Form.Label>
				        <Form.Control 
					        type="password" 
					        placeholder="Password"
					        value = {password1} 
					        onChange = {event => setPassword1(event.target.value)}
					        required
					        />
				      </Form.Group>

				      <Button variant="primary" type = "submit" onClick = {loginUser} disabled = {!isActive} className = "mt-1">
				        Login
				      </Button>
			    </Form>
			</Col>
		</Row>
		</Container>
	)
}