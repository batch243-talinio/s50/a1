import courseData from "../data/courses.js";
import CourseCard from '../components/CourseCard';
import {Container} from 'react-bootstrap';

export default function Courses() {
	// console.log(courseData);

	// Syntax: 'for getting the local storage data'
		// localStorage.getItem("propertyName")
	// const local = localStorage.getItem("email");
	// console.log(local)


	const courses = courseData.map(course => {
		return(
				<CourseCard key = {course.id} courseProp = {course}/>
			)
	})	

	return(
			<Container>
				{courses}
			</Container>
		)
} 