import {Navigate} from 'react-router-dom';
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout(){
	// Mini-activity:
		// once logout is clicked, it should automatically rerender the AppNavbar.

	const {setUser, unSetUser} = useContext(UserContext);
	setUser(localStorage.getItem("email"));

	unSetUser();

	useEffect(() => {
		setUser(localStorage.getItem("email"))
	})

	//localStorage.clear()
	return(
		<Navigate to = '/login' />
	)
}