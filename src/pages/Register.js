import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import {Container, Row, Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

export default function Register(){
	// State hooks to store the values of the input field from our user
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false)

	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
		/*console.log(email)
		console.log(password1)
		console.log(password2)*/
		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2){ 
			return setIsActive(true)
		}
		setIsActive(false)

	}, [email, password1, password2])

	function registerUser(event){
		event.preventDefault()
		alert(`Congratulations ${email}, you are now registered in our webiste!`)

		localStorage.setItem('email', email);

		setUser(localStorage.getItem("email"));

		setEmail('')
		setPassword1('')
		setPassword2('')
	}

	return (
		(user !== null) ?
		<Navigate to = '/'/>
		:
		<Container>
		<Row>
			<Col className = "col-md-4 col-8 offset-md-4 offset-2">
				<Form onSubmit = {registerUser} className = "bg-light">
				      <Form.Group className="mb-3" controlId="email">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
					        type="email" 
					        placeholder="Enter email"
					        value = {email} 
					        onChange = {event => setEmail(event.target.value)}
					        required
					        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Enter desired password</Form.Label>
				        <Form.Control 
					        type="password" 
					        placeholder="Password"
					        value = {password1} 
					        onChange = {event => setPassword1(event.target.value)}
					        required
					        />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="password2">
				        <Form.Label>Confirm password</Form.Label>
				        <Form.Control 
					        type="password" 
					        placeholder="Password" 
					        value = {password2}
					        onChange = {event => setPassword2(event.target.value)}
					        required
					        />
				      </Form.Group>

				      <Button variant="primary" type = "submit" onClick = {registerUser} disabled = {!isActive} className = "mt-1">
				        Register
				      </Button>
			    </Form>
			</Col>
		</Row>
		</Container>
	)
}