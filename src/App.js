// pages import
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import NotFound from './pages/NotFound';


//import {Fragment} from 'react';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import {useState} from 'react';


import {UserProvider} from './UserContext';


function App() {
  /*const name = 'John Smith';
  const element = <h1>Hello, {name}</h1>*/

  // state hook for the user that will be globally accessible using the useContext
  // this will also used to store the user information and beused for validating if a user is logged in on the app or not.

  const [user, setUser] = useState(localStorage.getItem("email"));

  // function for clearing user details
  const unSetUser = () => {
    localStorage.clear();
  }

  return (
    /*Router/BrowserRouter > Routes > Route*/
    <UserProvider value = {{user, setUser, unSetUser}}>
        <Router>
          <AppNavbar/>
          <Routes>
            <Route path = "/" element = {<Home/>}/>
            <Route path = "/courses" element = {<Courses/>}/>
            <Route path = "/login" element = {<Login/>}/>
            <Route path = "/register" element = {<Register/>}/>
            <Route path = "/logout" element = {<Logout/>}/>
            <Route path='*' element={<NotFound/>}/>
          </Routes>
        </Router>
    </UserProvider>
    
  );
}
export default App;
